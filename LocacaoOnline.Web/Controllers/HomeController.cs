﻿using LocacaoOnline.Infraestrutura.Data.Repositorios;
using LocacaoOnline.Web.Models;
using Rotativa.MVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LocacaoOnline.Web.Controllers
{
    [Authorize(Roles = "Administrador_Master, Funcionario")]
    public class HomeController : Controller
    {
        private readonly ClienteRepositorio _clienteRepositorio = new ClienteRepositorio();
        private readonly CacambaRepositorio _cacambaRepositorio = new CacambaRepositorio();
        private readonly LocacaoRepositorio _locacaoRepositorio = new LocacaoRepositorio();

        public ActionResult Index()
        {
            
            var totalClientes = _clienteRepositorio.BuscarTodos().Count();
            var totalCacambas = _cacambaRepositorio.BuscarTodos().Count();
            var totalLocacao = _locacaoRepositorio.BuscarTodos().Count();
            ViewBag.TotalClientes = totalClientes;
            ViewBag.TotalCacambas = totalCacambas;
            ViewBag.TotalLocacao = totalLocacao;

            return View();
        }

        
        public ActionResult Print()
        {
            return new ActionAsPdf("About", "customers");
        }
        
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View("_customers");
        }        
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}