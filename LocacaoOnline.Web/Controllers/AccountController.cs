﻿using AutoMapper;
using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Infraestrutura.Data.Repositorios;
using LocacaoOnline.Web.Util;
using LocacaoOnline.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace LocacaoOnline.Web.Controllers
{
    public class AccountController : Controller
    {
        private readonly FuncionarioRepositorio _funcionarioRepositorio = new FuncionarioRepositorio();

        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View(new LoginViewModel());
        }
        public ActionResult LogOff()
        {
            Session.Abandon();
            Session.RemoveAll();
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel login, string returnUrl = "")
        {
            if (!ModelState.IsValid)
            {
                return View(login);
            }            
            Funcionario model = new Funcionario()
            {
                Email = login.Email,
                Senha = login.Password
            };
            var logar = _funcionarioRepositorio.LogarFuncionario(model.Email, model.Senha);
            if (logar == null)
            {
                ModelState.AddModelError("", "Email ou Senha incorretos.");
                return View(login);
            }

            FormsAuthentication.SetAuthCookie(logar.Email, false);

            if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
            {
                return Redirect(returnUrl);
            }
            SessionManager.FuncionarioLogado = logar;
            Session["id"] = login.Email;
            return RedirectToAction("Index", "Home");

        }
        
    }
}