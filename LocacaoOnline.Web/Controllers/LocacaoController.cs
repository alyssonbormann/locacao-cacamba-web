﻿using AutoMapper;
using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Dominio.Models;
using LocacaoOnline.Infraestrutura.Data.Repositorios;
using LocacaoOnline.Web.Models;
using LocacaoOnline.Web.ViewModel;
using PagedList;
using System;
using System.Linq;
using System.Web.Mvc;

namespace LocacaoOnline.Web.Controllers
{
    [Authorize(Roles = "Administrador_Master, Funcionario")]
    public class LocacaoController : Controller
    {
        #region Repositorios
        private readonly LocacaoRepositorio _locacaoRepositorio = new LocacaoRepositorio();
        private readonly RecebimentoRepositorio _recebimentoRepositorio = new RecebimentoRepositorio();
        private readonly ClienteRepositorio _clienteRepositorio = new ClienteRepositorio();
        #endregion

        #region Get
        public ActionResult Index(string currentFilter, string pesquisar, int? pagina)
        {
            if (pesquisar != null)
            {
                pagina = 1;
            }
            else
            {
                pesquisar = currentFilter;
            }

            ViewBag.CurrentFilte = pesquisar;
            //Trazer apenas as locações que estão em Andamento e Vencida
            var locacao = from c in _locacaoRepositorio.BuscarTodos().OrderBy(c => c.Cliente.Nome).Where(c=>c.StatusLocacao != StatusLocacao.Concluida && c.StatusLocacao != StatusLocacao.Cancelada) select c;
            
            if (!String.IsNullOrEmpty(pesquisar))
            {
                locacao = locacao.Where(c => c.Cliente.Nome.ToUpper().Contains(pesquisar.ToUpper()));
            }
            int pageSize = 20;
            int pageNumber = (pagina ?? 1);

            return View(locacao.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult Concluida(string currentFilter, string pesquisar, int? pagina)
        {
            if (pesquisar != null)
            {
                pagina = 1;
            }
            else
            {
                pesquisar = currentFilter;
            }

            ViewBag.CurrentFilte = pesquisar;
            //Trazer apenas as locações concluidas
            var locacao = from c in _locacaoRepositorio.BuscarTodos().OrderBy(c => c.Cliente.Nome).Where(c => c.StatusLocacao == StatusLocacao.Concluida) select c;

            if (!String.IsNullOrEmpty(pesquisar))
            {
                locacao = locacao.Where(c => c.Cliente.Nome.ToUpper().Contains(pesquisar.ToUpper()));
            }
            int pageSize = 20;
            int pageNumber = (pagina ?? 1);

            return View(locacao.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult Cancelada(string currentFilter, string pesquisar, int? pagina)
        {
            if (pesquisar != null)
            {
                pagina = 1;
            }
            else
            {
                pesquisar = currentFilter;
            }

            ViewBag.CurrentFilte = pesquisar;
            //Trazer apenas as locações que estão Cancelada
            var locacao = from c in _locacaoRepositorio.BuscarTodos().OrderBy(c => c.Cliente.Nome).Where(c => c.StatusLocacao == StatusLocacao.Cancelada) select c;

            if (!String.IsNullOrEmpty(pesquisar))
            {
                locacao = locacao.Where(c => c.Cliente.Nome.ToUpper().Contains(pesquisar.ToUpper()));
            }
            int pageSize = 20;
            int pageNumber = (pagina ?? 1);

            return View(locacao.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult Detalhes(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }

                Locacao locacao = _locacaoRepositorio.BuscarPorId(id);
                var locacaoMapper = Mapper.Map<Locacao, LocacaoViewModel>(locacao);
                ViewBag.Locacao = locacao;
                
                var listaComNomeCacamba = new ItemRepositorio().BuscarTodos().Where(c => c.LocacaoId == id);
                ViewBag.ListaComNomeCacamba = listaComNomeCacamba.ToList();

                RecebimentoViewModel vm = new RecebimentoViewModel();
                vm.CalcularJurosPorDia(locacao);

                vm.LocacaoId = id;
                vm.DataCadastro = DateTime.Now;
                return View(vm);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }                
        public ActionResult Cacambas(int pagina = 1)
        {
            CacambaModel model = new CacambaModel
            {
                Cacambas = _locacaoRepositorio.ListaCacambas().OrderBy(c => c.StatusCacamba)
            };
            return View(model);
        }
        public RedirectToRouteResult Adicionar(Carrinho carrinho, Guid cacambaId, string returnUrl)
        {
            try
            {
                Cacamba cacamba = _locacaoRepositorio.ListaCacambas().FirstOrDefault(c => c.CacambaId == cacambaId);
                if (cacamba != null)
                {
                    if (cacamba.StatusCacamba == true)
                    {
                        carrinho.AdicionarItem(cacamba, 1);
                    }
                    else
                    {
                        TempData["mensagemStatusCacamba"] = string.Format("{0}", cacamba.Nome);
                        return RedirectToAction("Cacambas", "Locacao");
                    }
                }

                return RedirectToAction("Item", new { returnUrl });
            }
            catch (Exception e)
            {
                TempData["error"] = e.Message;
                return RedirectToAction("Cacambas");
            }
        }
        public RedirectToRouteResult RemoverItem(Carrinho carrinho, Guid cacambaId, string returnUrl)
        {
            Cacamba cacamba = _locacaoRepositorio.ListaCacambas().FirstOrDefault(c => c.CacambaId == cacambaId);
            if (cacamba != null)
            {
                carrinho.RemoverItem(cacamba);
            }
            return RedirectToAction("Item", new { returnUrl });
        }
        public PartialViewResult Resumo(Carrinho carrinho)
        {
            return PartialView(carrinho);
        }
        public ViewResult Item(Carrinho carrinho, string returnUrl)
        {
            return View(new CarrinhoViewModel
            {
                Carrinho = carrinho,
                ReturnUrl = returnUrl
            });
        }
        public ActionResult ConcluirLocacao()
        {
            var vm = new LocacaoViewModel();
            vm.ComboClienteId = _locacaoRepositorio.ListaClientes().Select(x => new SelectListItem { Text = x.Nome, Value = Convert.ToString(x.ClienteId) });
            return View(vm);
        }
        public ActionResult Recebimento(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }

                Locacao locacao = _locacaoRepositorio.BuscarPorId(id);
                var locacaoMapper = Mapper.Map<Locacao, LocacaoViewModel>(locacao);       
                ViewBag.Locacao = locacao;

                var listaComNomeCacamba = new ItemRepositorio().BuscarTodos().Where(c => c.LocacaoId == id);
                ViewBag.ListaComNomeCacamba = listaComNomeCacamba.ToList();

                RecebimentoViewModel vm = new RecebimentoViewModel();
                vm.CalcularJurosPorDia(locacao);

                vm.LocacaoId = id;
                vm.DataCadastro = DateTime.Now;
                return View(vm);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        public ActionResult Cancelamento(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }

                Locacao locacao = _locacaoRepositorio.BuscarPorId(id);
                var locacaoMapper = Mapper.Map<Locacao, LocacaoViewModel>(locacao);
                ViewBag.Locacao = locacao;
                
                var listaComNomeCacamba = new ItemRepositorio().BuscarTodos().Where(c => c.LocacaoId == id);
                ViewBag.ListaComNomeCacamba = listaComNomeCacamba.ToList();
                
                RecebimentoViewModel vm = new RecebimentoViewModel();
                vm.CalcularJurosPorDia(locacao);


                vm.LocacaoId = id;
                vm.DataCadastro = DateTime.Now;
                return View(vm);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }

        }
        public ActionResult EditarLocacao(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }
                Locacao locacao = _locacaoRepositorio.BuscarPorId(id);
                var locacaoMapper = Mapper.Map<Locacao, LocacaoViewModel>(locacao);               
                                
                return View(locacaoMapper);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        #endregion

        #region POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConcluirLocacao(Carrinho carrinho, LocacaoViewModel vm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(vm);
                }

                if (!carrinho.ItensCarrinho.Any())
                {
                    ModelState.AddModelError("", "Não foi possível concluir a locação, seu carrinho está vazio!");
                    return View(vm);
                }
                vm.ComboClienteId = _locacaoRepositorio.ListaClientes().Select(x => new SelectListItem { Text = x.Nome, Value = Convert.ToString(x.ClienteId) });

                if (vm.DataLocacao > vm.DataEntrega)
                {
                    ModelState.AddModelError("", "Data da Locação não pdoe ser maior que a data de entrega.");
                    return View(vm);
                }

                //Buscar todos os dados do Cliente pelo Id
                
                var listaClienteDisponivel = _locacaoRepositorio.BuscarTodos().FirstOrDefault(c => c.ClienteId == vm.ClienteId);
                
                //A lista tem que ser Diferente que Null
                if (listaClienteDisponivel != null)
                {
                    //Verficiar se o Cliente ta com alguma locação em andamento ou vencida
                    if (listaClienteDisponivel.StatusLocacao.ToString() == "EmAndamento" || listaClienteDisponivel.StatusLocacao.ToString() == "Vencida")
                    {
                        ModelState.AddModelError("", "Cliente já tem uma locação pendente.");
                        return View(vm);
                    }
                }
                var locacaoMapper = Mapper.Map<LocacaoViewModel, Locacao>(vm);
                locacaoMapper.LocacaoId = Guid.NewGuid();               
                if (locacaoMapper.DataEntrega < DateTime.Now)
                {
                    locacaoMapper.StatusLocacao = StatusLocacao.Vencida;
                }
                else
                {
                    locacaoMapper.StatusLocacao = StatusLocacao.EmAndamento;                    
                }
                _locacaoRepositorio.SalvarLocacao(carrinho, locacaoMapper);
                TempData["mensagem"] = "Locacao concluida com sucesso";
                carrinho.LimparCarrinho();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View(vm);
            }

        }
                
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Recebimento(RecebimentoViewModel vm, Guid id)
        {
            try
            {                                       
               
                Locacao locacao = _locacaoRepositorio.BuscarPorId(id);
                var locacaoMapper = Mapper.Map<Locacao, LocacaoViewModel>(locacao);
                ViewBag.Locacao = locacao;

                var listaComNomeCacamba = new ItemRepositorio().BuscarTodos().Where(c => c.LocacaoId == id);
                ViewBag.ListaComNomeCacamba = listaComNomeCacamba.ToList();
                
                vm.CalcularJurosPorDia(locacao);

                vm.LocacaoId = id;
                vm.DataCadastro = DateTime.Now;
                if (!ModelState.IsValid)
                {
                    return View(vm);
                }
                if (vm.ValorTotal < vm.ValorTotalJurosDias)
                {
                    ModelState.AddModelError("", "Valor do pagamento não pode ser menor que o valor Total.");
                    return View(vm);
                }
                
                var recebimentoMapper = Mapper.Map<RecebimentoViewModel, Recebimento>(vm);
                recebimentoMapper.RecebimentoId = Guid.NewGuid();
                _recebimentoRepositorio.SalvarRecebebimento(recebimentoMapper);
                TempData["mensagem"] = "Recebimento da Locação concluido !!";

                return RedirectToAction("Concluida");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();   
            }
        }

        [HttpPost, ActionName("Cancelamento")]
        [ValidateAntiForgeryToken]
        public ActionResult CancelamentoConfirmed(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }
                Locacao lo = _locacaoRepositorio.BuscarPorId(id);
                _locacaoRepositorio.SalvarCancelamento(lo);
                TempData["mensagem"] = "Cancelamento feito com sucesso !";
                
                //metodo para cancelar                
                return RedirectToAction("Cancelada");
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        
        #endregion
        public ActionResult Relatorio(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }

                Locacao locacao = _locacaoRepositorio.BuscarPorId(id);
                Recebimento recebimento = _recebimentoRepositorio.BuscarTodos().FirstOrDefault(c => c.Locacao.LocacaoId == id);
                
                var locacaoMapper = Mapper.Map<Locacao, LocacaoViewModel>(locacao);
                var recebimentoMapper = Mapper.Map<Recebimento, RecebimentoViewModel>(recebimento);

                ViewBag.Locacao = locacao;
                ViewBag.Recebimento = recebimento;                

                var listaComNomeCacamba = new ItemRepositorio().BuscarTodos().Where(c => c.LocacaoId == id);
                ViewBag.ListaComNomeCacamba = listaComNomeCacamba.ToList();

                RecebimentoViewModel vm = new RecebimentoViewModel();
                vm.CalcularJurosPorDia(locacao);

                vm.LocacaoId = id;
                vm.DataCadastro = DateTime.Now;
                return View(vm);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }

        }
    }
}