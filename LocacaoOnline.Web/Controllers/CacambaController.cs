﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Infraestrutura.Data.Repositorios;
using LocacaoOnline.Web.ViewModel;
using PagedList;
using System;
using System.Linq;
using System.Web.Mvc;
using Rotativa.MVC;
using AutoMapper;
using System.Collections.Generic;

namespace LocacaoOnline.Web.Controllers
{
    [Authorize(Roles = "Administrador_Master, Funcionario")]
    public class CacambaController : Controller
    {
        private readonly CacambaRepositorio _cacambaRepositorio = new CacambaRepositorio();

        #region Metodos_HttpGet
        public ActionResult Index(string currentFilter, string pesquisar, int? pagina)
        {
            if (pesquisar != null)
            {
                pagina = 1;
            }
            else
            {
                pesquisar = currentFilter;
            }

            ViewBag.CurrentFilte = pesquisar;

            var cacambas = Mapper.Map<IEnumerable<Cacamba>, IEnumerable<CacambaViewModel>>(_cacambaRepositorio.BuscarTodos());            

            if (!String.IsNullOrEmpty(pesquisar))
            {
                cacambas = cacambas.Where(c => c.Nome.ToUpper().Contains(pesquisar.ToUpper()));
            }
            int pageSize = 10;
            int pageNumber = (pagina ?? 1);

            return View(cacambas.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult Cadastrar()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        public ActionResult Editar(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }
                Cacamba cacamba = _cacambaRepositorio.BuscarTodos().FirstOrDefault(c => c.CacambaId == id);
                var cacambaMapper = Mapper.Map<Cacamba, CacambaViewModel>(cacamba);
                return View(cacambaMapper);
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }

                Cacamba cacamba = _cacambaRepositorio.BuscarPorId(id);
                var cacambaMapper = Mapper.Map<Cacamba, CacambaViewModel>(cacamba);
                return View(cacambaMapper);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        #endregion

        #region Metodos_HttpPost

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar(CacambaViewModel vm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(vm);
                }
                var verificarNomeCacamba = _cacambaRepositorio.VerficarNomeCacambaCadastrada(vm.Nome);
                if (verificarNomeCacamba != null)
                {
                    ModelState.AddModelError("", "O Nome já está em uso.");
                    return View(vm);
                }
                if (vm.VerificarPreco == false)
                {
                    ModelState.AddModelError("", "O Preço deve ser entre 10.00R$ e 200.00R$");
                    return View(vm);
                }

                var cacambaMapper = Mapper.Map<CacambaViewModel, Cacamba>(vm);            
                cacambaMapper.CacambaId = Guid.NewGuid();
                cacambaMapper.StatusCacamba = true;
                
                _cacambaRepositorio.Salvar(cacambaMapper);
                TempData["mensagem"] = "Cacamba cadastrada com sucesso !!";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(CacambaViewModel vm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(vm);
                }
                if (vm.VerificarPreco == false)
                {
                    ModelState.AddModelError("", "O Preço deve ser entre 10.00R$ e 200.00R$");
                    return View(vm);
                }

                var clienteMapper = Mapper.Map<CacambaViewModel, Cacamba>(vm);
                _cacambaRepositorio.Atualizar(clienteMapper);

                TempData["mensagem"] = string.Format("{0}", clienteMapper.Nome + ", alterada com sucesso !!");                

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }

                var cacamba = _cacambaRepositorio.BuscarPorId(id);
                _cacambaRepositorio.Deletar(cacamba);

                TempData["mensagem"] = "Cacamba deletada com sucesso !!";                

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        #endregion
       
        public ActionResult Print(Guid id)
        {
            Cacamba cacamba = _cacambaRepositorio.BuscarPorId(id);
            return new ActionAsPdf("Imprimir", cacamba);
        }
        public ActionResult Imprimir(Cacamba cacamba)
        {
            return View(cacamba);
        }      



        #region Relatorio
        //public ActionResult Imprimir(Guid id)
        //{
        //    Cacamba cacamba = _cacambaRepositorio.BuscarPorId(id);

        //    string htmlText = RenderViewToString("Imprimir", cacamba);


        //    byte[] buffer = RenderPDF(htmlText);

        //    return File(buffer, "application/PDF");            
        //}
        //private byte[] RenderPDF(string htmlText)
        //{
        //    byte[] renderedBuffer;

        //    const int HorizontalMargin = 40;
        //    const int VerticalMargin = 40;

        //    using (var outputMemoryStream = new MemoryStream())
        //    {
        //        using (var pdfDocument = new Document(PageSize.A4, HorizontalMargin, HorizontalMargin, VerticalMargin, VerticalMargin))
        //        {
        //            iTextSharp.text.pdf.PdfWriter pdfWriter = PdfWriter.GetInstance(pdfDocument, outputMemoryStream);
        //            pdfWriter.CloseStream = false;

        //            pdfDocument.Open();
        //            using (var htmlViewReader = new StringReader(htmlText))
        //            {
        //                XMLWorkerHelper.GetInstance().ParseXHtml(pdfWriter, pdfDocument, htmlViewReader);
        //            }

        //        }

        //        renderedBuffer = new byte[outputMemoryStream.Position];
        //        outputMemoryStream.Position = 0;
        //        outputMemoryStream.Read(renderedBuffer, 0, renderedBuffer.Length);
        //    }

        //    return renderedBuffer;
        //}
        //private string RenderViewToString(string viewName, object viewData)
        //{
        //    var renderedView = new StringBuilder();

        //    var controller = this;


        //    using (var responseWriter = new StringWriter(renderedView))
        //    {
        //        var fakeResponse = new HttpResponse(responseWriter);

        //        var fakeContext = new HttpContext(System.Web.HttpContext.Current.Request, fakeResponse);

        //        var fakeControllerContext = new ControllerContext(new HttpContextWrapper(fakeContext), controller.ControllerContext.RouteData,
        //            controller.ControllerContext.Controller);

        //        var oldContext = System.Web.HttpContext.Current;
        //        System.Web.HttpContext.Current = fakeContext;

        //        using (var viewPage = new ViewPage())
        //        {
        //            var viewContext = new ViewContext(fakeControllerContext, new FakeView(), new ViewDataDictionary(), new TempDataDictionary(), responseWriter);

        //            var html = new HtmlHelper(viewContext, viewPage);
        //            html.RenderPartial(viewName, viewData);
        //            System.Web.HttpContext.Current = oldContext;
        //        }
        //    }

        //    return renderedView.ToString();
        //}


        #endregion
    }
}