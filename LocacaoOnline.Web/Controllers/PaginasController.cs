﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LocacaoOnline.Web.Controllers
{
    public class PaginasController : Controller
    {
        // GET: Paginas
        public ActionResult Http404()
        {
            return View();
        }
        public ActionResult AcessoNegado()
        {
            return View();
        }
    }
}