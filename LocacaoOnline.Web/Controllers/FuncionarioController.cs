﻿using AutoMapper;
using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Infraestrutura.Data.Repositorios;
using LocacaoOnline.Web.Util;
using LocacaoOnline.Web.ViewModel;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LocacaoOnline.Web.Controllers
{
    [Authorize(Roles = "Administrador_Master")]
    public class FuncionarioController : Controller
    {
        private readonly FuncionarioRepositorio _funcionarioRepositorio = new FuncionarioRepositorio();

        #region Metodos_HttpGet

        public ActionResult Index(string currentFilter, string pesquisar, int? pagina)
        {
            if (pesquisar != null)
            {
                pagina = 1;
            }
            else
            {
                pesquisar = currentFilter;
            }
            ViewBag.CurrentFilter = pesquisar;

            var funcionarios = Mapper.Map<IEnumerable<Funcionario>, IEnumerable<FuncionarioViewModel>>(_funcionarioRepositorio.BuscarTodos());

            if (!String.IsNullOrEmpty(pesquisar))
            {
                funcionarios = funcionarios.Where(c => c.Nome.ToUpper().Contains(pesquisar.ToUpper()));
            }
            int pageSize = 10;
            int pageNumber = (pagina ?? 1);
            return View(funcionarios.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult Cadastrar()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        public ActionResult Editar(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }
                Funcionario funcionario = _funcionarioRepositorio.BuscarPorId(id);
                var funcionarioMapper = Mapper.Map<Funcionario, FuncionarioViewModel>(funcionario);
                
                return View(funcionarioMapper);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        public ActionResult TrocarSenha(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }
                Funcionario fun = _funcionarioRepositorio.BuscarPorId(id);
                AlterarSenha vm = new AlterarSenha()                
                {                    
                    FuncionarioId = fun.FuncionarioId,
                    Senha = fun.Senha
                
                };
                return View(vm);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }        
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }

                Funcionario funcionario = _funcionarioRepositorio.BuscarPorId(id);
                var funcionarioMapper = Mapper.Map<Funcionario, FuncionarioViewModel>(funcionario);
                
                return View(funcionarioMapper);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        public ActionResult Detalhes(Guid id)
        {
            try
            {
                Funcionario funcionario = _funcionarioRepositorio.BuscarPorId(id);
                var funcionarioMapper = Mapper.Map<Funcionario, FuncionarioViewModel>(funcionario);                
                return View(funcionarioMapper);

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        #endregion

        #region Metodos_HttoPost

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar(FuncionarioViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }
            var funcionarioExistente = _funcionarioRepositorio.RecuperarFuncionarioEmail(vm.Email);
            if (funcionarioExistente != null)
            {
                ModelState.AddModelError("", "Email está sendo utilizado.");
                return View(vm);
            }
            var validarCpf = _funcionarioRepositorio.VerificarCpfCadastrado(vm.Cpf);
            if (validarCpf != null)
            {
                ModelState.AddModelError("", "O CPF já está em uso.");
                return View(vm);
            }

            var funcionarioMapper = Mapper.Map<FuncionarioViewModel, Funcionario>(vm);
            funcionarioMapper.FuncionarioId = Guid.NewGuid();
            funcionarioMapper.SenhaKey = Functions.GetRandomString();
            _funcionarioRepositorio.CadastrarFuncionarioEncryp(funcionarioMapper);

            TempData["mensagem"] = "Funcionario cadastrado com sucesso !!";

            return RedirectToAction("Index");            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(FuncionarioViewModel vm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(vm);
                }

                var funcionarioMapper = Mapper.Map<FuncionarioViewModel, Funcionario>(vm);
                funcionarioMapper.SenhaKey = Functions.GetRandomString();               
                _funcionarioRepositorio.EditarSenhaFuncionarioEncryp(funcionarioMapper);

                TempData["mensagem"] = string.Format("{0}", vm.Nome + ", alterado com sucesso !!");                

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult TrocarSenha(AlterarSenha vm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(vm);
                }
                Funcionario fun = _funcionarioRepositorio.BuscarPorId(vm.FuncionarioId);

                _funcionarioRepositorio.EditarSenhaFuncionarioEncryp(
               new Dominio.Entidades.Funcionario()
               {                   
                   FuncionarioId = vm.FuncionarioId,
                   DataCadastro = fun.DataCadastro,
                   Nome = fun.Nome,
                   Cpf = fun.Cpf,
                   NivelAcesso = (NivelAcesso)fun.NivelAcesso,
                   Senha = vm.Senha,
                   SenhaKey = Functions.GetRandomString(),
                   Email = fun.Email,
                   Telefone = fun.Telefone,
                   Cep = fun.Cep,
                   Endereco = fun.Endereco,
                   Numero = fun.Numero,
                   Bairro = fun.Bairro,
                   Complemento = fun.Complemento
               });

                TempData["mensagem"] = "Senha alterada com sucesso !!";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }
                Funcionario funcionario = _funcionarioRepositorio.BuscarPorId(id);
                _funcionarioRepositorio.Deletar(funcionario);

                TempData["mensagem"] = "Funcionario deletado com sucesso !!";                

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }

        #endregion 
    }
}