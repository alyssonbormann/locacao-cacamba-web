﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Infraestrutura.Data.Repositorios;
using LocacaoOnline.Web.ViewModel;
using System;
using PagedList;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LocacaoOnline.Web.Models;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Collections;
using AutoMapper;

namespace LocacaoOnline.Web.Controllers
{
    [Authorize(Roles = "Administrador_Master, Funcionario")]
    public class ClienteController : Controller
    {
        private readonly ClienteRepositorio _clienteRepositorio = new ClienteRepositorio();

        #region Metodos_HttpGet
        public ActionResult Index(string currentFilter, string pesquisar, int? pagina)
        {
            if (pesquisar != null)
            {
                pagina = 1;
            }
            else
            {
                pesquisar = currentFilter;
            }

            ViewBag.CurrentFilte = pesquisar;
            var clientes = Mapper.Map<IEnumerable<Cliente>, IEnumerable<ClienteViewModel>>(_clienteRepositorio.BuscarTodos());

            if (!String.IsNullOrEmpty(pesquisar))
            {
                clientes = clientes.Where(c => c.Nome.ToUpper().Contains(pesquisar.ToUpper()));
            }
            int pageSize = 10;
            int pageNumber = (pagina ?? 1);

            return View(clientes.ToPagedList(pageNumber, pageSize));
        }
        public ActionResult Cadastrar()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        public ActionResult Editar(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }
                Cliente cliente = _clienteRepositorio.BuscarPorId(id);
                var clienteMapper = Mapper.Map<Cliente, ClienteViewModel>(cliente);
                                
                return View(clienteMapper);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }            
        }
        public ActionResult Delete(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }

                Cliente cliente = _clienteRepositorio.BuscarPorId(id);
                var clienteMapper = Mapper.Map<Cliente, ClienteViewModel>(cliente);
               
                return View(clienteMapper);
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        public ActionResult Detalhes(Guid id)
        {
            try
            {
                Cliente cliente = _clienteRepositorio.BuscarPorId(id);
                var clienteMapper = Mapper.Map<Cliente, ClienteViewModel>(cliente);                
                return View(clienteMapper);
            }
            catch (Exception ex)
            {

                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        #endregion

        #region Metodos_HttpPost
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cadastrar(ClienteViewModel vm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(vm);
                }

                var validarCpf = _clienteRepositorio.VerificarCpfCadastrado(vm.Cpf);
                if (validarCpf != null)
                {
                    ModelState.AddModelError("", "O CPF já está em uso.");
                    return View(vm);
                }

                var clienteMapper = Mapper.Map<ClienteViewModel, Cliente>(vm);
                clienteMapper.ClienteId = Guid.NewGuid();                
                _clienteRepositorio.Salvar(clienteMapper);

                TempData["mensagem"] = "Cliente cadastrado com sucesso !!";

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(ClienteViewModel vm)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return View(vm);
                }
                var clienteMapper = Mapper.Map<ClienteViewModel, Cliente>(vm);
                _clienteRepositorio.Atualizar(clienteMapper);

                TempData["mensagem"] = string.Format("{0}", clienteMapper.Nome + ", alterado com sucesso !!");                

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }           
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            try
            {
                if (id == null)
                {
                    return HttpNotFound();
                }
                Cliente cliente = _clienteRepositorio.BuscarPorId(id);                
                _clienteRepositorio.Deletar(cliente);

                TempData["mensagem"] = "Cliente deletado com sucesso !!";

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return View();
            }
        }
        #endregion

        #region Metodos_Apoio
        public ActionResult TotalClientesCadastrados()
        {
            var cli = from cliente in _clienteRepositorio.BuscarTodos()
                      group cliente by cliente.DataCadastro.Date
                      into dataGrupo
                      select new DataCadastroClienteGrupo()
                      {
                          DataCadastro = dataGrupo.Key,
                          contadorCadastroClientes = dataGrupo.Count()
                      };
            return View(cli.OrderBy(c => c.DataCadastro));
        }

        #endregion

        #region Relatorio

        

       
        #endregion
    }
}