﻿using LocacaoOnline.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocacaoOnline.Web.Models
{
    public class CacambaModel
    {
        public IEnumerable<Cacamba> Cacambas { get; set; }
    }
}