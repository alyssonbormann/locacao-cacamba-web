﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocacaoOnline.Web.Models
{
    public class LocacaoVM
    {        
        public Guid LocacaoId { get; set; }             
        public int Quantidade { get; set; }                     
        public DateTime DataLocacao { get; set; }
        public DateTime DataEntrega { get; set; }
        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Bairro { get; set; }
        public string Complemento { get; set; }
        public string NomeCliente { get; set; }
        public string NomeCamba { get; set; }
        public decimal Preco { get; set; }        
        public List<Item> ListaItems { get; set; }
        public decimal Total { get; set; }
        public double JurosPorDia { get; set; }
        public double SubTotal { get; set; }
        public int DiasAtraso { get; set; }
        public decimal ValorFinalComJuros { get; set; }
        public StatusLocacaoViewModel StatusLocacaoViewModel { get; set; }

        public decimal CalcularJuroTotal()
        {
            var x = (Convert.ToDecimal(JurosPorDia / 100) * Total) * DiasAtraso ;
            return x;
        }
    }
}