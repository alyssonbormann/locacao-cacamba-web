﻿
var map;
var directionsService = new google.maps.DirectionsService();
var info = new google.maps.InfoWindow({ maxWidth: 200 });

var marker = new google.maps.Marker({
    title: 'Google Porto Velho',
    position: new google.maps.LatLng('-8.751150', '-63.851210'),
});

function initialize() {
    var options = {
        zoom: 15,
        center: marker.position,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map($('#map_content')[0], options);

    marker.setMap(map);

    google.maps.event.addListener(marker, 'click', function () {
        info.setContent('<h5>Empresa SERVMAQ</h1> <br>Av: Calama, 4633 - Flodoaldo Pontes Pinto, Porto Velho - RO, Brasil');
        info.open(map, marker);
    });
}

$(document).ready(function () {
    $('#form_route').submit(function () {
        info.close();
        marker.setMap(null);

        var directionsDisplay = new google.maps.DirectionsRenderer();
        var endereco = $("#Endereco").val();
        var numero = $("#Numero").val();
        var bairro = $("#Bairro").val();
        var request = {
            origin: endereco + numero + bairro,
            destination: marker.position,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
                directionsDisplay.setMap(map);
            }
        });

        return false;
    });
});

