﻿using LocacaoOnline.Dominio.Models;
using System.Web.Mvc;

namespace LocacaoOnline.Web.Infraestrutura
{
    public class CarrinhoModelBinder : System.Web.Mvc.IModelBinder
    {
        private const string SessionKey = "Carrinho";

        public object BindModel(ControllerContext controllerContext, System.Web.Mvc.ModelBindingContext bindingContext)
        {
            //Obtenho o carrinho da session

            Carrinho carrinho = null;

            if (controllerContext.HttpContext.Session != null)
            {
                carrinho = (Carrinho)controllerContext.HttpContext.Session[SessionKey];
            }

            //Crio o carrinho se não tenho a session
            if (carrinho == null)
            {
                carrinho = new Carrinho();

                if (controllerContext.HttpContext.Session != null)
                {
                    controllerContext.HttpContext.Session[SessionKey] = carrinho;
                }
            }

            //Retorno do carrinho
            return carrinho;
        }
    }
}