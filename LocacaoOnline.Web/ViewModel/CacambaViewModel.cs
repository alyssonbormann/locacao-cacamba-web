﻿using LocacaoOnline.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace LocacaoOnline.Web.ViewModel
{
    public class CacambaViewModel
    {
        public Guid CacambaId { get; set; }
        public DateTime DataCadastro { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public decimal Preco { get; set; }        
        public string Descricao { get; set; }
        public bool StatusCacamba { get; set; }
        public virtual List<Item> Item { get; set; }
        public bool VerificarPreco
        {
            get
            {
                if (Preco < 10.00M || Preco > 200.00M)
                {
                    return false;
                }
                return true;
            }            
        }
    }
}