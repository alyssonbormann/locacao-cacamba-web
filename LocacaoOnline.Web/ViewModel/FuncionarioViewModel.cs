﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LocacaoOnline.Web.ViewModel
{
    public class FuncionarioViewModel
    {
        public Guid FuncionarioId { get; set; }
        public DateTime DataCadastro { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Cpf { get; set; }
        [EnumDataType(typeof(NivelAcessoViewModel),ErrorMessage ="Selecione o Nível de Acesso.")]
        public NivelAcessoViewModel NivelAcesso { get; set; }

        [Required]
        [StringLength(10, MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar a senha")]
        [System.ComponentModel.DataAnnotations.Compare("Senha", ErrorMessage = "As senhas não conferem.")]
        public string ConfirmaSenha { get; set; }

        [Required]
        public string Email { get; set; }
        [Required]
        public string Telefone { get; set; }
        public string Cep { get; set; }
        [Required]
        public string Endereco { get; set; }
        [Required]
        public string Numero { get; set; }
        [Required]
        public string Bairro { get; set; }
        public string Complemento { get; set; }
        
    }    
    public enum NivelAcessoViewModel
    {
        Administrador_Master = 1,
        Funcionario = 2,
    }

    public class AlterarSomenteDadosFuncionario
    {
        public Guid FuncionarioId { get; set; }        
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Cpf { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public NivelAcessoViewModel NivelAcesso { get; set; }        
        [Required]
        public string Telefone { get; set; }
        public string Cep { get; set; }
        [Required]
        public string Endereco { get; set; }
        [Required]
        public string Numero { get; set; }
        [Required]
        public string Bairro { get; set; }
        public string Complemento { get; set; }
    }
    public class AlterarSenha
    {
        public Guid FuncionarioId { get; set; }       
        [Required]
        [StringLength(10, MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar a senha")]
        [System.ComponentModel.DataAnnotations.Compare("Senha", ErrorMessage = "As senhas não conferem.")]
        public string ConfirmaSenha { get; set; }

        public string SenhaKey { get; set; }
    }
}
