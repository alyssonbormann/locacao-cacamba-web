﻿using LocacaoOnline.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LocacaoOnline.Web.ViewModel
{
    public class LocacaoViewModel
    {        
        public Guid LocacaoId { get; set; }
        public DateTime DataCadastro { get; set; }
        [Required]
        public Guid ClienteId { get; set; }        
        [Required]
        public DateTime DataLocacao { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime DataEntrega { get; set; }
        [Required]
        public double JurosPorDia { get; set; }        
        public StatusLocacaoViewModel StatusLocacaoViewModel { get; set; }        
        public string Cep { get; set; }
        [Required]
        public string Endereco { get; set; }
        [Required]
        public string Numero { get; set; }
        [Required]
        public string Bairro { get; set; }
        public string Complemento { get; set; }
        public IEnumerable<SelectListItem> ComboClienteId { get; set; }
        public List<Item> Items { get; set; }        
        public virtual Cliente Cliente { get; set; }        
       
    }
    public enum StatusLocacaoViewModel
    {
        [Display(Name = "Em Andamento")]
        EmAndamento = 1,
        [Display(Name = "Vencida")]
        Vencida = 2,
        [Display(Name = "Concluida")]
        Concluida = 3,
        [Display(Name = "Cancelada")]
        Cancelada = 4        
    }
}