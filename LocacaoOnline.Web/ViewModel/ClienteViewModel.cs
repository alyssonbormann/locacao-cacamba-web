﻿using LocacaoOnline.Web.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LocacaoOnline.Web.ViewModel
{
    public class ClienteViewModel
    {        
        public Guid ClienteId { get; set; }
        [Required]
        public string Nome { get; set; }
        [Required]
        [CustomValidationCPF(ErrorMessage = "CPF inválido")]
        public string Cpf { get; set; }
        public string Telefone { get; set; }
        [Required]
        public string Email { get; set; }        
        public string Cep { get; set; }
        [Required]
        public string Endereco { get; set; }
        [Required]
        public string Bairro { get; set; }
        [Required]
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public DateTime DataCadastro { get; set; }
    }
}