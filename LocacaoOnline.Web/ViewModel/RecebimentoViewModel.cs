﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Infraestrutura.Data.Repositorios;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LocacaoOnline.Web.ViewModel
{
    public class RecebimentoViewModel
    {        
        public Guid RecebimentoId { get; set; }
        public Guid LocacaoId { get; set; }
        public DateTime DataCadastro { get; set; }
        [Required]
        public decimal ValorTotal { get; set; }
        [EnumDataType(typeof(FormaPagamentoViewModel), ErrorMessage = "Selecione a Forma de Pagamento.")]
        public FormaPagamentoViewModel FormaPagamentoVieModel { get; set; }


        public int DiasAtraso { get; set; }
        public decimal ValorTotalJurosDias { get; set; }        

        public void CalcularJurosPorDia(Locacao locacao)
        {
            LocacaoRepositorio _locacaoRepositorio = new LocacaoRepositorio();

            var lo = _locacaoRepositorio.BuscarPorId(locacao.LocacaoId);
            int totalDias = 0;
            if (lo.DataEntrega < DateTime.Now)
            {
                TimeSpan data = Convert.ToDateTime(DateTime.Now) - Convert.ToDateTime(lo.DataEntrega);
                totalDias = data.Days;
            }
            else
            {
                //TimeSpan data = Convert.ToDateTime(DateTime.Now) - Convert.ToDateTime(lo.DataEntrega);
                //totalDias = data.Days;
                totalDias = 0;
            }
            List<Decimal> listaPrecoTotal = new List<decimal>();
            foreach (var item in lo.Item)
            {
                listaPrecoTotal.Add(item.Cacamba.Preco);
            }
            

            double juros = Math.Truncate(1.02);
            juros = lo.JurosPorDia;
            decimal precoCacamba = listaPrecoTotal.Sum();
            decimal resultado = precoCacamba + (precoCacamba * (Convert.ToDecimal(juros / 100) * totalDias));

            DiasAtraso = totalDias;
            ValorTotalJurosDias = Convert.ToDecimal(resultado.ToString("n2"));
        }

        
    }
    public enum FormaPagamentoViewModel
    {
        Dinheiro = 1,
        Debito = 2,
        Credito = 3,
        Cheque = 4
    }
}