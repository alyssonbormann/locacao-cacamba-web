﻿using LocacaoOnline.Dominio.Models;

namespace LocacaoOnline.Web.ViewModel
{
    public class CarrinhoViewModel
    {
        public Carrinho Carrinho { get; set; }

        public string ReturnUrl { get; set; }
    }
}