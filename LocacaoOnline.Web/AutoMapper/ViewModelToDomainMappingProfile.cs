﻿using AutoMapper;
using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Web.ViewModel;

namespace LocacaoOnline.Web.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<Cliente, ClienteViewModel>();
            Mapper.CreateMap<Cacamba, CacambaViewModel>();
            Mapper.CreateMap<Locacao, LocacaoViewModel>();
            Mapper.CreateMap<Funcionario, FuncionarioViewModel>();
            Mapper.CreateMap<Recebimento, RecebimentoViewModel>();
        }
    }
}