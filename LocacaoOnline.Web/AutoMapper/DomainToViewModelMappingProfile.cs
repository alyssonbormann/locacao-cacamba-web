﻿using AutoMapper;
using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Web.ViewModel;

namespace LocacaoOnline.Web.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<ClienteViewModel, Cliente>();
            Mapper.CreateMap<CacambaViewModel, Cacamba>();
            Mapper.CreateMap<LocacaoViewModel, Locacao>();
            Mapper.CreateMap<FuncionarioViewModel, Funcionario>();
            Mapper.CreateMap<RecebimentoViewModel, Recebimento>();
        }
    }
}