﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Web.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LocacaoOnline.Web.Util
{
    public class SessionManager
    {
        public static Funcionario FuncionarioLogado
        {
            set
            {

                HttpContext.Current.Session.Add("FuncionarioLogado", value);
            }
            get
            {
                return (Funcionario)HttpContext.Current.Session["FuncionarioLogado"];
            }

        }

        public static bool IsAuthenticated
        {
            get
            {
                return ((Funcionario)HttpContext.Current.Session["FuncionarioLogado"]) != null;
            }
        }
    }
}