﻿using System.Web;
using System.Web.Optimization;

namespace LocacaoOnline.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-2.2.3.js"));            

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/methods_pt.js"));


            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                       "~/Scripts/bootstrap.js",
                       "~/Scripts/bootstrap-select.js",
                       "~/Scripts/jquery.slimscroll.js",
                       "~/Scripts/waves.js",
                       "~/Scripts/jquery.slimscroll.js",
                       "~/Scripts/admin.js",
                       "~/Scripts/demo.js",
                       "~/Scripts/jquery.maskMoney.min.js",
                       "~/Scripts/jquery.maskedinput.js",
                       "~/Scripts/moment.min.js",
                       "~/Scripts/bootstrap-datetimepicker.min.js"));



            bundles.Add(new StyleBundle("~/Content/css").Include(
             "~/Content/bootstrap.css",
             "~/Content/bootstrap-datetimepicker.css",
             "~/Content/font-awesome.min.css",
             "~/Content/waves.css",
             "~/Content/animate.css",
             "~/Content/md-preloader.css",
             "~/Content/style.css",
             "~/Content/all-themes.css"));



            bundles.Add(new StyleBundle("~/Content/google").Include(
            "~/Content/material.min.css"));

            bundles.Add(new ScriptBundle("~/Scripts/google").Include(
                    "~/Content/material.min.js"));



            bundles.Add(new StyleBundle("~/Content/cssLoca").Include(
                    "~/Cotent/select2.css"));

            bundles.Add(new ScriptBundle("~/Scripts/jsLoca").Include(
                    "~/Scripts/material.min.js",
                    "~/Scripts/Select2.js"));
        }
    }
}
