﻿using LocacaoOnline.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocacaoOnline.Infraestrutura.Data.EntityConfig
{
    public class ClienteConfig : EntityTypeConfiguration<Cliente>
    {//
        public ClienteConfig()
        {
            HasKey(c => c.ClienteId);

            Property(c => c.Nome)
                .IsRequired();

            Property(c => c.Cpf)
                .IsRequired();

            Property(c => c.Telefone)
                .IsRequired();

            Property(c => c.Email)
                .IsRequired();

            Property(c => c.Endereco)
                .IsRequired();

            Property(c => c.Bairro)
                .IsRequired();

            Property(c => c.Numero)
                .IsRequired();
        }
    }
}
