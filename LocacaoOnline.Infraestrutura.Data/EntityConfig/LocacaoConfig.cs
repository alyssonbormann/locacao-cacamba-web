﻿using LocacaoOnline.Dominio.Entidades;
using System.Data.Entity.ModelConfiguration;

namespace LocacaoOnline.Infraestrutura.Data.EntityConfig
{
    public class LocacaoConfig : EntityTypeConfiguration<Locacao>
    {
        public LocacaoConfig()
        {
            HasKey(j => j.LocacaoId);

            Property(j => j.Endereco)
                .IsRequired();

            Property(j => j.Numero)
                .IsRequired();

            Property(j => j.Bairro)
                .IsRequired();

            HasRequired(j => j.Cliente)
                .WithMany()
                .Map(m => m.MapKey("ClienteId"));
        }
    }
}
