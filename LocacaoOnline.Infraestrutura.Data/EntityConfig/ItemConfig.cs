﻿using LocacaoOnline.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocacaoOnline.Infraestrutura.Data.EntityConfig
{
    public class ItemConfig: EntityTypeConfiguration<Item>
    {
        public ItemConfig()
        {
            HasKey(j => j.ItemId);

            Property(j => j.Quantidade)
                .IsRequired();

            HasRequired(j => j.Locacao)
                .WithMany() // Locação pode ter muitos items
                .Map(m => m.MapKey("LocacaoId")); //a chave estrangeira em Item é LocacaoId

            HasRequired(j => j.Cacamba)
                .WithMany() //Cacamba pode ter uma lista de items
                .Map(m => m.MapKey("CacambaId")); //a chave estrangeira é CacambaId

        }
    }
}
