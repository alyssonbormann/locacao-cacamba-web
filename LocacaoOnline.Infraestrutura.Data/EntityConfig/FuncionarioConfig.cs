﻿using LocacaoOnline.Dominio.Entidades;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;

namespace LocacaoOnline.Infraestrutura.Data.EntityConfig
{
    public class FuncionarioConfig : EntityTypeConfiguration<Funcionario>
    {
        public FuncionarioConfig()
        {
            HasKey(c => c.FuncionarioId);

            Property(c => c.Nome)
                .IsRequired();

            Property(c => c.Email)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnAnnotation(
                 IndexAnnotation.AnnotationName,
                 new IndexAnnotation(
                    new IndexAttribute("IX_LoginNameUser", 1) { IsUnique = true }));

            Property(c => c.Senha)
                .IsRequired()
                .HasMaxLength(2048);
        }
    }
}
