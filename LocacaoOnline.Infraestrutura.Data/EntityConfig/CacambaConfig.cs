﻿using LocacaoOnline.Dominio.Entidades;
using System.Data.Entity.ModelConfiguration;

namespace LocacaoOnline.Infraestrutura.Data.EntityConfig
{
    public class CacambaConfig : EntityTypeConfiguration<Cacamba>
    {
        public CacambaConfig()
        {
            HasKey(c => c.CacambaId);

            Property(c => c.Nome)
                .IsRequired();

            Property(c => c.Preco)
                .IsRequired();

        }
    }
}
