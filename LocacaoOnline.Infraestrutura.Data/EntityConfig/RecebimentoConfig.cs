﻿using LocacaoOnline.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocacaoOnline.Infraestrutura.Data.EntityConfig
{
    public class RecebimentoConfig : EntityTypeConfiguration<Recebimento>
    {
        public RecebimentoConfig()
        {
            HasKey(r => r.RecebimentoId);

            HasRequired(r => r.Locacao)
                .WithMany()
                .Map(m => m.MapKey("LocacaoId"));
        }
    }
}
