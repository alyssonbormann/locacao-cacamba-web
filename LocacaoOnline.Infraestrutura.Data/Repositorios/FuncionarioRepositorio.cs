﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Dominio.Interfaces;
using LocacaoOnline.Infraestrutura.Data.Seguranca;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocacaoOnline.Infraestrutura.Data.Repositorios
{
    public class FuncionarioRepositorio : RepositorioBase<Funcionario>, IFuncionarioRepositorio
    {
        public Funcionario CadastrarFuncionarioEncryp(Funcionario funcionario)
        {
            funcionario.Senha = Crypto.EncryptStringAES(funcionario.Senha, funcionario.SenhaKey);
            Salvar(funcionario);
            return funcionario;
        }

        public Funcionario EditarSenhaFuncionarioEncryp(Funcionario funcionario)
        {
            funcionario.Senha = Crypto.EncryptStringAES(funcionario.Senha, funcionario.SenhaKey);
            Atualizar(funcionario);
            return funcionario;
        }

        public Funcionario LogarFuncionario(string email, string senha)
        {
            var funcionario = Db.Funcionarios.Where(f => f.Email == email).FirstOrDefault();
            if (funcionario == null)
            {
                return null;
            }
            string passDecrypt = Crypto.DecryptStringAES(funcionario.Senha, funcionario.SenhaKey);
            if (passDecrypt == senha)
            {
                return funcionario;
            }
            else
            {
                return null;
            }
        }

        public Funcionario RecuperarFuncionarioEmail(string email)
        {
            var funcionario = Db.Funcionarios.Where(f => f.Email == email).FirstOrDefault();
            return funcionario;
        }

        public Funcionario VerificarCpfCadastrado(string cpf)
        {
            var funcionario = Db.Funcionarios.Where(c => c.Cpf == cpf).FirstOrDefault();
            return funcionario;
        }
    }
}
