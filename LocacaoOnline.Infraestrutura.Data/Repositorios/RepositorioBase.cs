﻿using LocacaoOnline.Dominio.Interfaces;
using LocacaoOnline.Infraestrutura.Data.Contexto;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace LocacaoOnline.Infraestrutura.Data.Repositorios
{
    public class RepositorioBase<T> : IDisposable, IRepositorioBase<T> where T : class
    {
        protected ProjetoContexto Db = new ProjetoContexto();

        public void Atualizar(T obj)
        {
            Db.Entry(obj).State = EntityState.Modified;
            Db.SaveChanges();
        }

        public T BuscarPorId(Guid id)
        {
            return Db.Set<T>().Find(id);
        }

        public IEnumerable<T> BuscarTodos()
        {
            return Db.Set<T>().ToList();
        }

        public void Deletar(T obj)
        {
            Db.Set<T>().Remove(obj);
            Db.SaveChanges();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void Salvar(T obj)
        {
            Db.Set<T>().Add(obj);
            Db.SaveChanges();
        }
    }
}
