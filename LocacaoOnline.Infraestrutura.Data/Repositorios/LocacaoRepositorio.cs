﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Dominio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LocacaoOnline.Dominio.Models;

namespace LocacaoOnline.Infraestrutura.Data.Repositorios
{
    public class LocacaoRepositorio : RepositorioBase<Locacao>, ILocacaoRepositorio
    {        
        public IEnumerable<Cacamba> ListaCacambas()
        {
            return Db.Cacambas;
        }        

        public IEnumerable<Cliente> ListaClientes()
        {
            return Db.Clientes;
        }
        public Cacamba MudarStatusCacambaFalse(Guid id)
        {
            Cacamba ca = Db.Cacambas.Find(id);
            ca.StatusCacamba = false;
            Db.Entry(ca).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();
            return ca;
        }
        public Cacamba MudarStatusCacambaTrue(Guid id)
        {
            Cacamba ca = Db.Cacambas.Find(id);
            ca.StatusCacamba = true;
            Db.Entry(ca).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();
            return ca;
        }

        public void SalvarLocacao(Carrinho carrinho, Locacao locacao)
        {
            Item items = new Item();
            Db.Locacaos.Add(locacao);
            foreach (var item in carrinho.ItensCarrinho)
            {
                MudarStatusCacambaFalse(item.CacambaId);
                items.LocacaoId = locacao.LocacaoId;
                items.CacambaId = item.CacambaId;
                items.Quantidade = item.Quantidade;
                Db.Items.Add(items);
                Db.SaveChanges();
            }

            Db.SaveChanges();
        }
        public void SalvarCancelamento(Locacao locacao)
        {

            var lo = Db.Locacaos.FirstOrDefault(c => c.LocacaoId == locacao.LocacaoId);

            //trazer todos os item que tem o id da locacao
            var item = Db.Items.Where(c => c.LocacaoId == lo.LocacaoId);
            foreach (var i in item.ToList())
            {
                // para cada item que tem na lista, mudar o status da cacamba para true(disponivel)
                MudarStatusCacambaTrue(i.CacambaId);
                Db.SaveChanges();
            }
            //Ao finalizar, mudar o status da locacao para concluido
            StatusLocacaoCancelada(locacao.LocacaoId);
            Db.SaveChanges();

        }

        public Locacao StatusLocacaoCancelada(Guid id)
        {
            Locacao lo = Db.Locacaos.Find(id);
            lo.StatusLocacao = StatusLocacao.Cancelada;
            Db.Entry(lo).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();
            return lo;
        }

        
    }
}
