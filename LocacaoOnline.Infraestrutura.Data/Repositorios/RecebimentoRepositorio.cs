﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Dominio.Interfaces;
using System;
using System.Linq;

namespace LocacaoOnline.Infraestrutura.Data.Repositorios
{
    public class RecebimentoRepositorio : RepositorioBase<Recebimento>, IRecebimentoRepositorio
    {
        public Cacamba MudarStatusCacamba(Guid id)
        {
            Cacamba ca = Db.Cacambas.Find(id);
            ca.StatusCacamba = true;
            Db.Entry(ca).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();
            return ca;
        }

        public Locacao MudarStatusLocacaoConcluida(Guid id)
        {
            Locacao lo = Db.Locacaos.Find(id);
            lo.StatusLocacao = StatusLocacao.Concluida;
            Db.Entry(lo).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();
            return lo;
        }

        public void SalvarRecebebimento(Recebimento recebimento)
        {                        
            //pegar o Id da Locacao que ta vindo
            var lo = Db.Locacaos.FirstOrDefault(c => c.LocacaoId == recebimento.LocacaoId);

            //trazer todos os item que tem o id da locacao
            var item = Db.Items.Where(c => c.LocacaoId == lo.LocacaoId);            
            foreach(var i in item.ToList())
            {
                // para cada item que tem na lista, mudar o status da cacamba para true(disponivel)
                MudarStatusCacamba(i.CacambaId);
                Db.SaveChanges();
            }
            //Ao finalizar, mudar o status da locacao para concluido
            MudarStatusLocacaoConcluida(recebimento.LocacaoId);
            recebimento.RecebimentoId = Guid.NewGuid();
            Db.Recebimentos.Add(recebimento);
            Db.SaveChanges();
        }
    }
}
