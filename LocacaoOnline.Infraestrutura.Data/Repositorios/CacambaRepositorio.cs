﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Dominio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocacaoOnline.Infraestrutura.Data.Repositorios
{
    public class CacambaRepositorio : RepositorioBase<Cacamba>, ICacambaRepositorio
    {
        public Cacamba VerficarNomeCacambaCadastrada(string nome)
        {
            var nomeCacamba = Db.Cacambas.FirstOrDefault(c => c.Nome == nome);
            return nomeCacamba;

        }
    }
}
