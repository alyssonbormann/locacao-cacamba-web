﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Dominio.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocacaoOnline.Infraestrutura.Data.Repositorios
{
    public class ClienteRepositorio : RepositorioBase<Cliente>, IClienteRepositorio
    {
        public IQueryable<Cliente> RelatorioClienteId(Guid id)
        {
            var clientes = Db.Clientes.Where(c => c.ClienteId == id);
            return clientes;
        }

        public Cliente VerificarCpfCadastrado(string cpf)
        {
            var cliente = Db.Clientes.Where(c => c.Cpf == cpf).FirstOrDefault();
            return cliente;
        }
    }
}
