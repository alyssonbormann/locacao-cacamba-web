﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Infraestrutura.Data.EntityConfig;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocacaoOnline.Infraestrutura.Data.Contexto
{
    public class ProjetoContexto : DbContext
    {
        public ProjetoContexto() : base("BancoContexto") { }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Cacamba> Cacambas { get; set; }
        public DbSet<Funcionario> Funcionarios { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Locacao> Locacaos { get; set; }
        public DbSet<Recebimento> Recebimentos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties()
                .Where(c => c.Name == c.ReflectedType.Name + "Id")
                .Configure(c => c.IsKey());

            modelBuilder.Properties<string>()
                .Configure(c => c.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(c => c.HasMaxLength(100));

            modelBuilder.Configurations.Add(new CacambaConfig());
            modelBuilder.Configurations.Add(new ClienteConfig());
            //modelBuilder.Configurations.Add(new FuncionarioConfig());
            //modelBuilder.Configurations.Add(new ItemConfig());
            //modelBuilder.Configurations.Add(new LocacaoConfig());
            //modelBuilder.Configurations.Add(new RecebimentoConfig());

        }
        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("DataCadastro") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("DataCadastro").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("DataCadastro").IsModified = false;
                }
            }
            return base.SaveChanges();
        }
    }
}
