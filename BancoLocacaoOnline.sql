USE [master]
GO
/****** Object:  Database [BancoLocacaoOnline]    Script Date: 30/09/2016 11:33:22 ******/
CREATE DATABASE [BancoLocacaoOnline]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BancoLocacaoOnline', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\BancoLocacaoOnline.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BancoLocacaoOnline_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\BancoLocacaoOnline_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BancoLocacaoOnline] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BancoLocacaoOnline].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BancoLocacaoOnline] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET ARITHABORT OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [BancoLocacaoOnline] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BancoLocacaoOnline] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BancoLocacaoOnline] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BancoLocacaoOnline] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BancoLocacaoOnline] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET RECOVERY FULL 
GO
ALTER DATABASE [BancoLocacaoOnline] SET  MULTI_USER 
GO
ALTER DATABASE [BancoLocacaoOnline] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BancoLocacaoOnline] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BancoLocacaoOnline] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BancoLocacaoOnline] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [BancoLocacaoOnline]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 30/09/2016 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cacamba]    Script Date: 30/09/2016 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cacamba](
	[CacambaId] [uniqueidentifier] NOT NULL,
	[DataCadastro] [datetime] NOT NULL,
	[Nome] [varchar](100) NULL,
	[Preco] [decimal](18, 2) NOT NULL,
	[Descricao] [varchar](100) NULL,
	[StatusCacamba] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Cacamba] PRIMARY KEY CLUSTERED 
(
	[CacambaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 30/09/2016 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[ClienteId] [uniqueidentifier] NOT NULL,
	[Nome] [varchar](100) NOT NULL,
	[Cpf] [varchar](100) NOT NULL,
	[Telefone] [varchar](100) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Cep] [varchar](100) NULL,
	[Endereco] [varchar](100) NOT NULL,
	[Bairro] [varchar](100) NOT NULL,
	[Numero] [varchar](100) NOT NULL,
	[Complemento] [varchar](100) NULL,
	[DataCadastro] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.Cliente] PRIMARY KEY CLUSTERED 
(
	[ClienteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Funcionario]    Script Date: 30/09/2016 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Funcionario](
	[FuncionarioId] [uniqueidentifier] NOT NULL,
	[DataCadastro] [datetime] NOT NULL,
	[Nome] [varchar](100) NULL,
	[Cpf] [varchar](100) NULL,
	[Senha] [varchar](100) NULL,
	[SenhaKey] [varchar](100) NULL,
	[NivelAcesso] [int] NOT NULL,
	[Email] [varchar](100) NULL,
	[Telefone] [varchar](100) NULL,
	[Cep] [varchar](100) NULL,
	[Endereco] [varchar](100) NULL,
	[Numero] [varchar](100) NULL,
	[Bairro] [varchar](100) NULL,
	[Complemento] [varchar](100) NULL,
 CONSTRAINT [PK_dbo.Funcionario] PRIMARY KEY CLUSTERED 
(
	[FuncionarioId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Item]    Script Date: 30/09/2016 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[LocacaoId] [uniqueidentifier] NOT NULL,
	[CacambaId] [uniqueidentifier] NOT NULL,
	[Quantidade] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Item] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Locacao]    Script Date: 30/09/2016 11:33:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Locacao](
	[LocacaoId] [uniqueidentifier] NOT NULL,
	[DataCadastro] [datetime] NOT NULL,
	[DataLocacao] [datetime] NOT NULL,
	[DataEntrega] [datetime] NOT NULL,
	[Cep] [varchar](100) NULL,
	[Endereco] [varchar](100) NULL,
	[Numero] [varchar](100) NULL,
	[Bairro] [varchar](100) NULL,
	[Complemento] [varchar](100) NULL,
	[ClienteId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_dbo.Locacao] PRIMARY KEY CLUSTERED 
(
	[LocacaoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Index [IX_CacambaId]    Script Date: 30/09/2016 11:33:22 ******/
CREATE NONCLUSTERED INDEX [IX_CacambaId] ON [dbo].[Item]
(
	[CacambaId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LocacaoId]    Script Date: 30/09/2016 11:33:22 ******/
CREATE NONCLUSTERED INDEX [IX_LocacaoId] ON [dbo].[Item]
(
	[LocacaoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ClienteId]    Script Date: 30/09/2016 11:33:22 ******/
CREATE NONCLUSTERED INDEX [IX_ClienteId] ON [dbo].[Locacao]
(
	[ClienteId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Item_dbo.Cacamba_CacambaId] FOREIGN KEY([CacambaId])
REFERENCES [dbo].[Cacamba] ([CacambaId])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_dbo.Item_dbo.Cacamba_CacambaId]
GO
ALTER TABLE [dbo].[Item]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Item_dbo.Locacao_LocacaoId] FOREIGN KEY([LocacaoId])
REFERENCES [dbo].[Locacao] ([LocacaoId])
GO
ALTER TABLE [dbo].[Item] CHECK CONSTRAINT [FK_dbo.Item_dbo.Locacao_LocacaoId]
GO
ALTER TABLE [dbo].[Locacao]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Locacao_dbo.Cliente_Cliente_ClienteId] FOREIGN KEY([ClienteId])
REFERENCES [dbo].[Cliente] ([ClienteId])
GO
ALTER TABLE [dbo].[Locacao] CHECK CONSTRAINT [FK_dbo.Locacao_dbo.Cliente_Cliente_ClienteId]
GO
USE [master]
GO
ALTER DATABASE [BancoLocacaoOnline] SET  READ_WRITE 
GO
