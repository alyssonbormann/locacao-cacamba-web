﻿using System;

namespace LocacaoOnline.Dominio.Entidades
{
    public class Item
    {
        public int ItemId { get; set; }
        public Guid LocacaoId { get; set; }
        public Guid CacambaId { get; set; }
        public int Quantidade { get; set; }
        public virtual Locacao Locacao { get; set; }
        public virtual Cacamba Cacamba { get; set; }
    }
}