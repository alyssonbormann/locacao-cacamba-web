﻿using System;
using System.Collections.Generic;

namespace LocacaoOnline.Dominio.Entidades
{
    public class Cacamba
    {
        public Guid CacambaId { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public string Descricao { get; set; }
        public bool StatusCacamba { get; set; }
        public virtual List<Item> Item { get; set; }
    }
}
