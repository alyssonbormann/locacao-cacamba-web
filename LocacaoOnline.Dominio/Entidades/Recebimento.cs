﻿using System;

namespace LocacaoOnline.Dominio.Entidades
{
    public class Recebimento
    {
        public Guid RecebimentoId { get; set; }
        public Guid LocacaoId { get; set; }
        public DateTime DataCadastro { get; set; }
        public decimal ValorTotal { get; set; }
        public virtual Locacao Locacao { get; set; }
        public FormaPagamento FormaPagamento { get; set; }
    }

    public enum FormaPagamento
    {
        Dinheiro = 1,
        Debito = 2,
        Credito = 3,
        Cheque = 4
    }
}
