﻿using System;

namespace LocacaoOnline.Dominio.Entidades
{
    public class Funcionario
    {
        public Guid FuncionarioId { get; set; }
        public DateTime DataCadastro { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Senha { get; set; }
        public string SenhaKey { get; set; }
        public NivelAcesso NivelAcesso { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Bairro { get; set; }
        public string Complemento { get; set; }
    }
    public enum NivelAcesso
    {
        Administrador_Master = 1,
        Funcionario = 2
    }
}
