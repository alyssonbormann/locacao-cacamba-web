﻿using System;
using System.Collections.Generic;

namespace LocacaoOnline.Dominio.Entidades
{
    public class Locacao
    {
        public Locacao()
        {
            this.Item = new List<Item>();
        }
        public Guid LocacaoId { get; set; }
        public DateTime DataCadastro { get; set; }
        public Guid ClienteId { get; set; }
        public DateTime DataLocacao { get; set; }
        public DateTime DataEntrega { get; set; }
        public double JurosPorDia { get; set; }
        public string Cep { get; set; }
        public string Endereco { get; set; }
        public string Numero { get; set; }
        public string Bairro { get; set; }
        public string Complemento { get; set; }
        public StatusLocacao StatusLocacao { get; set; }
        public virtual List<Item> Item { get; set; }
        public virtual Cliente Cliente { get; set; }
        
    }

    public enum StatusLocacao
    {
        EmAndamento = 1,
        Vencida = 2,
        Concluida = 3,
        Cancelada = 4
    }
}