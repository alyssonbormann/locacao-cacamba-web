﻿using LocacaoOnline.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LocacaoOnline.Dominio.Models
{
    public class Carrinho
    {
        protected readonly List<Item> _ItemCarrinho = new List<Item>();

        //Adicionar
        public void AdicionarItem(Cacamba cacamba, int quantidade)
        {
            Item item = _ItemCarrinho.FirstOrDefault(c => c.Cacamba.CacambaId == cacamba.CacambaId);

            if (item == null)
            {
                _ItemCarrinho.Add(new Item
                {
                    Cacamba = cacamba,
                    CacambaId = cacamba.CacambaId,
                    Quantidade = quantidade
                });

            }
            else
            {
                throw new Exception("Está caçamba já foi adicionada.");
                //item.Quantidade += quantidade;
            }
        }

        //Remover Item
        public void RemoverItem(Cacamba cacamba)
        {
            _ItemCarrinho.RemoveAll(l => l.Cacamba.CacambaId == cacamba.CacambaId);
        }

        //Obter o valor total
        public decimal ObterValorTotal()
        {
            return _ItemCarrinho.Sum(e => e.Cacamba.Preco * e.Quantidade);
        }

        //Limpar Carrinho
        public void LimparCarrinho()
        {
            _ItemCarrinho.Clear();
        }

        //Itens carrinho
        public IEnumerable<Item> ItensCarrinho
        {
            get { return _ItemCarrinho; }
        }
    }
}
