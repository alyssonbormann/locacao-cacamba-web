﻿using LocacaoOnline.Dominio.Entidades;

namespace LocacaoOnline.Dominio.Interfaces
{
    public interface IFuncionarioRepositorio : IRepositorioBase<Funcionario>
    {
        Funcionario RecuperarFuncionarioEmail(string email);
        Funcionario LogarFuncionario(string email, string senha);
        Funcionario CadastrarFuncionarioEncryp(Funcionario funcionario);
        Funcionario EditarSenhaFuncionarioEncryp(Funcionario funcionario);
        Funcionario VerificarCpfCadastrado(string cpf);
    }
}
