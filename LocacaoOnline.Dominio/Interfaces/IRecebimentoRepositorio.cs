﻿using LocacaoOnline.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocacaoOnline.Dominio.Interfaces
{
    public interface IRecebimentoRepositorio : IRepositorioBase<Recebimento>
    {
        Cacamba MudarStatusCacamba(Guid id);
        Locacao MudarStatusLocacaoConcluida(Guid id);
        void SalvarRecebebimento(Recebimento recebimento);
    }
}
