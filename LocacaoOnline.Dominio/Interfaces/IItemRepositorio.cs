﻿using LocacaoOnline.Dominio.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocacaoOnline.Dominio.Interfaces
{
    public interface IItemRepositorio:IRepositorioBase<Item>
    {

    }
}
