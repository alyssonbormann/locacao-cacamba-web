﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocacaoOnline.Dominio.Interfaces
{
    public interface IRepositorioBase<T> where T : class
    {
        void Salvar(T obj);
        T BuscarPorId(Guid id);
        IEnumerable<T> BuscarTodos();
        void Atualizar(T obj);
        void Deletar(T obj);
        void Dispose();
    }
}
