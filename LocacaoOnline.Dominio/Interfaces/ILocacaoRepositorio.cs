﻿using LocacaoOnline.Dominio.Entidades;
using LocacaoOnline.Dominio.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocacaoOnline.Dominio.Interfaces
{
    public interface ILocacaoRepositorio : IRepositorioBase<Locacao>
    {
        IEnumerable<Cacamba> ListaCacambas();
        IEnumerable<Cliente> ListaClientes();
        void SalvarLocacao(Carrinho carrinho, Locacao locacao);
        Cacamba MudarStatusCacambaFalse(Guid id);
        Cacamba MudarStatusCacambaTrue(Guid id);
        Locacao StatusLocacaoCancelada(Guid id);
        void SalvarCancelamento(Locacao locacao);        

    }
}
